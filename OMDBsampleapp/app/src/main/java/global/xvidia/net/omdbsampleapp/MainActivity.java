package global.xvidia.net.omdbsampleapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import global.xvidia.net.omdbsampleapp.utils.AppConsatants;

public class MainActivity extends AppCompatActivity {

    private Button buttonSearch;
    private EditText movieName, movieYear;
    private TextView title;
    private ProgressBar mProgressView;
    private Context context;
    private LinearLayout layout;
    private Toast toast;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        buttonSearch = (Button) findViewById(R.id.search);
        movieName = (EditText) findViewById(R.id.movieName);
        movieYear = (EditText) findViewById(R.id.movieYear);
        title = (TextView) findViewById(R.id.textView);

        mProgressView = (ProgressBar) findViewById(R.id.login_progress);
        mProgressView.setVisibility(View.GONE);

        layout = new LinearLayout(this);
        toast = new Toast(this);
        tv = new TextView(this);
        tv.setPadding(10, 10, 10, 10);
//        layout.setBackground(ContextCompat.getDrawable(context,R.drawable.toast));
        layout.setBackgroundResource(R.color.colorAccent);
        tv.setTextColor(ContextCompat.getColor(context, R.color.white));
        tv.setTextSize(15);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        layout.addView(tv);
        toast.setView(layout);
        toast.setGravity(Gravity.BOTTOM, 0, 50);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = movieName.getText().toString();
                String year = movieYear.getText().toString();
                if (name != null && year != null) {
                    if (name.length() > 0) {
                        String url;
                        showProgress(true);
                        if (year.equalsIgnoreCase("")) {
                            url = "http://www.omdbapi.com/?t=" + name + "&y=&plot=long&r=json";
                        }else{
                            url = "http://www.omdbapi.com/?t=" + name + "&y" + year + "=&plot=long&r=json";
                        }
                        String finalUrl = url.replace(" ","+");
                        sendSearchRequest(finalUrl);

                    } else {
                        new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Error")
                                .setMessage("Errrrr...Enter movie details.")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }

                                })
                                .show();
                        // Position you toast here toast position is 50 dp from bottom you can give any integral value

                    }

                }
            }
        });

    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation rotation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate);
                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void sendSearchRequest(String url) {

        try {

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    showProgress(false);
                    try {
                        if (response.getString("Response").equals("True")) {
                            String title = response.getString("Title");
                            String year = response.getString("Year");
                            String released = response.getString("Released");
                            String runtime = response.getString("Runtime");
                            String genre = response.getString("Genre");
                            String plot = response.getString("Plot");
                            String actors = response.getString("Actors");
                            String director = response.getString("Director");
                            String writer = response.getString("Writer");
                            String language = response.getString("Language");
                            String country = response.getString("Country");
                            String awards = response.getString("Awards");
                            String poster = response.getString("Poster");
                            String metascore = response.getString("Metascore");
                            String votes = response.getString("imdbVotes");
                            String imdbRating = response.getString("imdbRating");
                            Intent mIntent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                            Bundle mBundle = new Bundle();
                            mBundle.putString(AppConsatants.TITLE, title);
                            mBundle.putString(AppConsatants.YEAR, year);
                            mBundle.putString(AppConsatants.RELEASED, released);
                            mBundle.putString(AppConsatants.RUNTIME, runtime);
                            mBundle.putString(AppConsatants.GENRE, genre);
                            mBundle.putString(AppConsatants.PLOT, plot);
                            mBundle.putString(AppConsatants.ACTORS, actors);
                            mBundle.putString(AppConsatants.DIRECTOR, director);
                            mBundle.putString(AppConsatants.WRITER, writer);
                            mBundle.putString(AppConsatants.LANGUAGE, language);
                            mBundle.putString(AppConsatants.COUNTRY, country);
                            mBundle.putString(AppConsatants.AWARD, awards);
                            mBundle.putString(AppConsatants.POSTER, poster);
                            mBundle.putString(AppConsatants.IMDBRATING, imdbRating);
                            mBundle.putString(AppConsatants.IMDBVOTES, votes);
                            mBundle.putString(AppConsatants.METASCORE, metascore);
                            mIntent.putExtras(mBundle);
                            startActivity(mIntent);
                        } else {
                            String error = response.getString("Error");
                            tv.setText(error);
                            toast.show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    showProgress(false);

                }
            });
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
