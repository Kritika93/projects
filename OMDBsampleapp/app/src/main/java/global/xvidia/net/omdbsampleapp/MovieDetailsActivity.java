package global.xvidia.net.omdbsampleapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.net.URL;

import global.xvidia.net.omdbsampleapp.utils.AppConsatants;

/**
 * Created by vasu on 4/5/16.
 */
public class MovieDetailsActivity extends AppCompatActivity {

    private ImageView img;
    private Bitmap bitmap;
    private ProgressBar pDialog;
    private ProgressDialog dialog;
    private TextView plotSummary,tvGenre, tvRuntime, tvMetascore, tvLanguage, tvCountry, tvAwards,
            tvVotes, tvImdbRating,tvRelease, tvMovieName,tvActors,tvDirector, tvWriter, tvQrCode;


    private String title, year, released,imdbVotes,metascore, runtime, genre, plot, actors, director, writer, language, country, awards, poster, imdbRating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        pDialog = (ProgressBar) findViewById(R.id.pro);
        tvQrCode = (TextView) findViewById(R.id.qrCode);
        dialog = new ProgressDialog(MovieDetailsActivity.this);
        dialog.setMessage("Loading....");
        tvQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                Intent mIntent = new Intent(MovieDetailsActivity.this, DecoderActivity.class);
                startActivity(mIntent);
            }
        });

        img = (ImageView) findViewById(R.id.poster);
        plotSummary = (TextView) findViewById(R.id.plot);
        tvGenre = (TextView) findViewById(R.id.genre);
        tvRuntime = (TextView) findViewById(R.id.runtime);
        tvMetascore = (TextView) findViewById(R.id.metascore);
        tvImdbRating = (TextView) findViewById(R.id.rating);
        tvVotes = (TextView) findViewById(R.id.votes);
        tvRelease = (TextView) findViewById(R.id.releaseDate);
        tvMovieName = (TextView) findViewById(R.id.movieName);
        tvLanguage = (TextView) findViewById(R.id.language);
        tvCountry = (TextView) findViewById(R.id.country);
        tvAwards = (TextView) findViewById(R.id.awards);
        tvActors = (TextView) findViewById(R.id.actors);
        tvDirector = (TextView) findViewById(R.id.director);
        tvWriter = (TextView) findViewById(R.id.writer);
        Intent intent = getIntent();
        if (null != intent) { //Null Checking
            title = intent.getStringExtra(AppConsatants.TITLE);
            year = intent.getStringExtra(AppConsatants.YEAR);
            released = intent.getStringExtra(AppConsatants.RELEASED);
            runtime = intent.getStringExtra(AppConsatants.RUNTIME);
            genre = intent.getStringExtra(AppConsatants.GENRE);
            plot = intent.getStringExtra(AppConsatants.PLOT);
            actors = intent.getStringExtra(AppConsatants.ACTORS);
            director = intent.getStringExtra(AppConsatants.DIRECTOR);
            writer = intent.getStringExtra(AppConsatants.WRITER);
            language = intent.getStringExtra(AppConsatants.LANGUAGE);
            country = intent.getStringExtra(AppConsatants.COUNTRY);
            awards = intent.getStringExtra(AppConsatants.AWARD);
            poster = intent.getStringExtra(AppConsatants.POSTER);
            imdbRating = intent.getStringExtra(AppConsatants.IMDBRATING);
            imdbVotes = intent.getStringExtra(AppConsatants.IMDBVOTES);
            metascore = intent.getStringExtra(AppConsatants.METASCORE);
        }
        plotSummary.setText(plot);
        tvGenre.setText(genre);
        tvRuntime.setText(runtime);
        tvVotes.setText(imdbVotes + " Votes");
        tvMetascore.setText(metascore);
        tvImdbRating.setText(imdbRating+" /10");
        tvRelease.setText(released);
        tvMovieName.setText(title +" ("+year+")");
        tvLanguage.setText(language);
        tvCountry.setText(country);
        tvAwards.setText(awards);
        tvActors.setText(actors);
        tvDirector.setText(director);
        tvWriter.setText(writer);

//        new LoadImage().execute(poster);
        new DownloadImageTask(img)
                .execute(poster);

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           pDialog.setVisibility(View.VISIBLE);

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception ex) {
                String err = (ex.getMessage()==null)?"SD Card failed":ex.getMessage();
                Log.e("error",err);
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                bmImage.setImageBitmap(result);
                pDialog.setVisibility(View.GONE);

            } else {

               pDialog.setVisibility(View.GONE);
                Toast.makeText(MovieDetailsActivity.this, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

            }
        }
    }

   /* private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MovieDetailsActivity.this);
            pDialog.setMessage("Loading Image ....");
            pDialog.show();

        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if (image != null) {
                img.setImageBitmap(image);
                pDialog.dismiss();

            } else {

                pDialog.dismiss();
                Toast.makeText(MovieDetailsActivity.this, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

            }
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        dialog.dismiss();
    }
}
