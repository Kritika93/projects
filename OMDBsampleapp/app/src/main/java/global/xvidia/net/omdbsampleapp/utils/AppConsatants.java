package global.xvidia.net.omdbsampleapp.utils;

/**
 * Created by Ravi_office on 17-Dec-15.
 */
public class AppConsatants {

    public final static String TITLE = "TITLE";
    public final static String YEAR = "YEAR";
    public final static String RELEASED = "RELEASED";
    public final static String RUNTIME = "RUNTIME";
    public final static String GENRE = "GENRE";
    public final static String PLOT = "PLOT";
    public final static String ACTORS = "ACTORS";
    public final static String DIRECTOR = "DIRECTOR";
    public final static String WRITER = "WRITER";
    public final static String LANGUAGE = "LANGUAGE";
    public final static String COUNTRY = "COUNTRY";
    public final static String AWARD = "AWARD";
    public final static String POSTER = "POSTER";
    public final static String IMDBRATING = "IMDBRATING";
    public final static String IMDBVOTES = "IMDBVOTES";
    public final static String METASCORE = "METASCORE";

/*TEST CONSTANTS*/

//    public final static String TEST_USERNAME = "USERPerson101@gmail.com";
}
